{
  description = "A collection of templates by myself for myself";

  inputs = {
    nixpkgs.url = "nixpkgs";

    alejandra.url = "github:kamadorueda/alejandra";
    alejandra.inputs.nixpkgs.follows = "nixpkgs";

    devshell.url = "github:numtide/devshell";
    devshell.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {
    self,
    alejandra,
    devshell,
    nixpkgs,
  }: let
    lib = nixpkgs.lib;

    mkOutputs = builtins.mapAttrs (
      _: attr:
        if builtins.isFunction attr
        then
          builtins.listToAttrs (map (system: {
              name = system;
              value = attr {
                inherit system;
                pkgs = import nixpkgs {
                  inherit system;
                  overlays = [alejandra.overlay devshell.overlay];
                };
              };
            })
            lib.systems.flakeExposed)
        else attr
    );
  in
    mkOutputs {
      templates.default = self.templates.flake;

      /*
       Nix
       */
      templates.flake = {
        path = "${self}/nix/simple";
        description = "A simple nix flake";
      };

      /*
       Java
       */
      templates.fabric-mod = {
        path = "${self}/java/fabricmc";
        description = "A simple fabric minecraft mod";
      };

      /*
       Rust
       */
      templates.rust-bin = {
        path = "${self}/rust/simple/bin";
        description = "A simple rust binary using nix-cargo-integration";
      };
      templates.rust-lib = {
        path = "${self}/rust/simple/lib";
        description = "A simple rust library using nix-cargo-integration";
      };
      templates.rust-combined = {
        path = "${self}/rust/simple/combined";
        description = "A simple rust package with a binary and a library using nix-cargo-integration";
      };
      templates.bevy-app = {
        path = "${self}/rust/bevy/app";
        description = "A bevy app using nix-cargo-integration";
      };
      templates.bevy-plugin = {
        path = "${self}/rust/bevy/plugin";
        description = "A bevy plugin using nix-cargo-integration";
      };
      templates.bevy-project = {
        path = "${self}/rust/bevy/project";
        description = "A bevy project workspace with several crates using nix-cargo-integration";
      };

      devShells = {
        system,
        pkgs,
        ...
      }: {
        default = self.devShells.${system}.dev;
        dev = pkgs.devshell.mkShell ({pkgs, ...}: {
          devshell.name = "flake-templates";
          commands = [
            {
              package = pkgs.alejandra;
              category = "tools";
            }
          ];
        });
        ci = pkgs.devshell.mkShell ({pkgs, ...}: {
          devshell.name = "flake-templates-CI";
          commands = [
            {
              package = pkgs.alejandra;
              category = "tools";
            }
          ];
        });
      };
    };
}
