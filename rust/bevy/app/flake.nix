{
  inputs = {
    nixpkgs.url = "nixpkgs";
    nci.url = "github:yusdacra/nix-cargo-integration";
    nci.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {
    self,
    nci,
    nixpkgs,
    ...
  }: nci.lib.makeOutputs {
    root = ./.;
    systems = nixpkgs.lib.systems.flakeExposed;

    defaultOutputs.package = "example";
    defaultOutputs.app = "example";

    overrides.shell = common: prev: with common.pkgs; prev // {
      devshell.packages = [gdb];
      commands = prev.commands ++ [
        { name = "rust-gdb"; package = common.rustToolchain.rustc; category = "tools";}
        { package = linuxPackages.perf; category = "tools";}
      ];
    };
  };
}

