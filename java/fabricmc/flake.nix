{
  inputs = {
    nixpkgs.url = "nixpkgs";
    alejandra.url = "github:kamadorueda/alejandra";
    alejandra.inputs.nixpkgs.follows = "nixpkgs";
    devshell.url = "github:numtide/devshell";
    devshell.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {self, alejandra, devshell, nixpkgs, ...}: let
    lib = nixpkgs.lib;

    mkOutputs = builtins.mapAttrs (
      _: attr:
        if builtins.isFunction attr
        then
          builtins.listToAttrs (map (system: {
              name = system;
              value = attr {
                inherit system;
                pkgs = import nixpkgs {
                  inherit system;
                  overlays = [alejandra.overlay devshell.overlay];
                };
              };
            })
            lib.systems.flakeExposed)
        else attr
    );
  in mkOutputs {
    packages = {system, ...}: {
      default = self.packages.${system}.fabric-example-mod;
      fabric-example-mod = builtins.derivation {
        name = "fabric-example-mod";
        inherit system;
        builder = ''
          echo "Gradle build unsupported for now"
          exit 1
        '';
      };
    };
    devShells = {pkgs, system, ...}: {
      default = self.devShells.${system}.dev;
      dev = pkgs.devshell.mkShell ({pkgs, ...}: {
        devshell.name = "fabric-example-mod";
        commands = with pkgs; [
          { package = jdk17; category = "java"; }
          { package = gradle; category = "java"; }
        ];
        env = [
          { name = "JAVA_HOME"; eval = "$DEVSHELL_DIR"; }
          { name = "GRADLE_HOME"; eval = "$PRJ_DATA_DIR/gradle"; }
        ];
      });
    };
  };
}
